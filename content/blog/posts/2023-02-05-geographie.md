---
date: 2023-02-05
tags:
  - background
---

# Géographie

Roanoke est une petite, vraiment petite, île. La ville de Kitty Hawk, de taille raisonnable, est assez éloignée, et sur une autre île qui plus est...

<!-- more -->

La ville est suffisamment éloignée pour que rejoindre Roanoke en vélo soit compliqué pour des enfants et des adolescents.

Il y a bien la ville de _Manteo_, siège du comté de Dare qui englobe l'île Roanoke, ou _Manchese_, un lieu dit un peu plus au sud, voire _Fort Raleigh City_ au nord, mais ces noms ne sont pas vraiment vendeurs...

Une seule solution : transférer la ville de Kitty Hawk sur l'île Roanoke, agrandir cette dernière pour l'occasion pour que tous les évènements puissent s'y dérouler, que les lieux majeures puisse y être sans problème pour les relier.

La base militaire, un centre de recherche, tout devra être accessible par des enfants ou des adolescents. Un peu de nage pourquoi pas, surtout si c'est un lieu fréquenté des jeunes, mais ça ne doit pas être une distance supérieure à 1 mile.
Ces quelques retouches rendront les évènements moins proches de la réalité, mais la partie n'en sera que plus simple.

En parlant de lieux, quels sont les lieux typiques pour ce genre d'aventure ?

- Un centre ville, d'une petite ville américaine avec tout ce qui compte de petits magasins : l'épicerie, le magasin d'électronique, le bar, etc.
- Un plan d'eau, rivière, étang, lac, lac artificiel (beaucoup de possibilités liées à ça dans le temps), un lieu où les jeunes vont s'amuser et nager, mais qui possède ses propres légendes.
- Un lieu souterrain, une grotte marine, un réseau relié aux égouts, qui mènerait près de la mer
- Une forêt évidemment, plus ou moins dense selon les besoins
- un centre commercial pour les époques les plus récentes
- Un collège et un lycée, avec tout ce qui vient avec, le stade, le terrain de base-ball.
- Une bibliothèque dans un pur style colonial, façon Nouvelle-Angleterre, tour à tour imposante, impressionnante, menaçante, majestueuse...
- Un quartier de banlieue américaine, où les enfants se déplacent à vélo, les mères cuisines des tartes et les pères tondent la pelouse et lavent leur voiture achetée à crédit, comme leur maison d'ailleurs.
- Le centre de recherche et les mystères qui l'entourent
- La base militaire. Non c'est tout.
- Une maison abandonnée, entourée de légendes qu'on se raconte pour se faire peur. elle peut être en ville, dans la forêt, sur une falaise...
- Une maison isolée. la maison isolée n'est pas forcément la maison abandonnée, même si elle peut également être abandonnée... Ce qui change ? L'absence de légende ?
- Une falaise d'ailleurs. Un front de mer accidenté, qui pourrait cacher certaines choses. Potentiellement relié au lieu souterrain. Le tout pouvant être relié au centre de recherche, ou la base militaire. Bref, à voir selon les besoins.
- Une centrale nucléaire, mais je suis pas convaincu.
- Une entreprise industrielle qui possède la ville par les emplois qu'elle fournit. Peut être la centrale nucléaire. L'idée est surtout qu'elle pourrait cacher certaines choses. Ou être une source, supposée ou non, des évènements surnaturels.
- La riche demeure du grand notable du coin. Ça peut être un sénateur, le directeur de l'entreprise ou la centrale...
- Une ferme. Ou plusieurs. Avec des gens à la fois plus proches de la nature, plus éloignés de la ville, et de ses habitants. Plus bourrus, ils peuvent faire de bons candidats pour finir comme suspects. Ou d'excellentes victimes qu'on pourra mettre du temps à découvrir.
