---
date: 2022-09-11
tags:
  - story_behind
---

# Bande son

Dans un scénario cinématographique, la bande son est fondamentale pour poser l'ambiance sur les scènes descriptives.

<!-- more -->

Et la scène de ce type la plus récurrente, c'est la scène d'ouverture. On pose une ambiance, une époque. L'accompagnement sonore va donner le marqueur aux joueurs, donner la référence que tout le monde possède, et partage. C'est une bonne partie du travail. Le reste étant une manière de présenter la scène : survol d'une ville par beau temps, caméra posée au sol dans une rue passante... Chaque époque possède son style cinématographique.

Sans me poser de contrainte sur les époques qui seront choisies pour la campagne, j'ai commencé une playlist contenant des chansons qui pour moi sont tellement représentatives d'une époque, qu'elles viennent avec des images, des couleurs, un style. Idéales, toujours selon moi, pour une scène d'ouverture d'une nouvelle époque.

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/playlist/2ct8r2AeQmRuAW3sdTqLyU?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>
