---
date: 2022-12-23
tags:
  - scenario
---

# Grandes lignes

La campagne se déroulant autour de Roanoke, il faudra jouer avec la légende. Voici l'idée.

<!-- more -->

## Historique

À l'époque de la colonie de Roanoke, une tribu locale vérénait un Dieu très ancien et ne voyait pas d'un bon œil l'installation de colons sur leur sol sacré...

Il s'est ensuivi des évènements étranges qui frappèrent la colonie et ses habitants, des choses étranges et terrifiantes. Mortelles aussi.

Un groupe de colons investigateurs a mené l'enquête et découvert le culte des indigènes, ainsi que leur totem difforme qui servait de canaliseur pour utiliser le pouvoir de leur Dieu. Ils étudièrent à distance le culte pour comprendre son fonctionnement, et notèrent tout ce qui pourrait leur servir à se protéger, sans succès.

Seule option restante, le groupe a volé ce totem pour le détruire. Ils n'y sont cependant pas arrivé, et l'ont ramené dans la colonie. Mais la colère des indigènes s'est très vite déchaînée, et ils ont mené un raid sur la colonie, pour récupérer leur totem, et accessoirement massacrer l'envahisseur.

La colonie a tenté de se défendre comme elle pouvait. Le sang versé a commencé à libérer le Dieu très ancien, et devant cette scène, un des investigateurs a donné le recueil de leur étude du culte à un enfant, avant de l'envoyer loin de la colonie.

Acculés, les investigateurs ont utilisé un sort de scellement pour empêcher le retour du Dieu, mais l'effet ne fut pas celui escompté. Plutôt que le totem, c'est la colonie qui fut scellée. Le réveil du Dieu n'a pas pu être empêché.

Son arrivée a provoqué une onde de chaleur, faisant fondre les yeux et la chaire (cette scène sera vécue en vue subjective par les investigateurs) et transformant les survivants, colons comme indigènes, en tas de chair brûlée, vivant et hurlant de douleur.

En arrivant, le Dieu a pris possession d'un des investigateurs. Le scellement de la colonie a eu pour effet de bloquer ses pouvoir, et il se retrouva lui aussi bloqué dans un corps fondu, le rendant fou de douleur. Jusqu'à ce que...

L'enfant a réussit à s'enfuir de Roanoke, emmenant avec lui le recueil qui servirait à essayer de ramener la colonie. Il ignorait tout de ce qui s'était passé à l'intérieur du scellement.

Il consacra toute sa vie aux recherches occultes, réussissant à combattre le vieillissement, et à tenter des rituels régulièrement, sans succès (et tuant des victimes à intervalle régulier également). Il a poursuivi son objectif, devenu obsession, jusqu'à croiser les investigateurs.

## Partie 1

La campagne commence au début d'un cycle dans les années 50 ou 60. Les investigateurs sont enfants.

L'enfant échappé de Roanoke, appelons le _le rescapé_, commence un nouveau cycle pour essayer de libérer la colonie. Il a pour ça besoin de sacrifices. Avec le temps, il a affiné sa méthode, et cible désormais les enfants, plus faciles à maîtriser, dont les amis des investigateurs.

Ces derniers se retrouveront à mener l'enquête, et, si tout va bien, ils se retrouveront confontés au kidnappeur d'enfant. Toutefois, ce sera un détournement d'attention du rescapé, qui reviendra à la charge.

## Partie 2

De nombreuses années se sont écoulées. Les enfants sont désormais de grands adolescents terminant le lycée, alors que les disparitions d'enfants recommencent. Cette fois le rescapé sera contraint d'avancer à visage découvert, c'est à ce moment que les investigateurs se rendront compte qu'il ne s'agit pas juste de meurtres en série, mais bien d'une histoire occulte.

Lors d'une scène finale, si tout va bien, les adolescents accompagné d'un de leurs parents arriveront sur le lieu du rituel. Le parent sera tué, amenant le sang manquant au rescapé pour terminer le rituel. Si les investigateurs y arrivent, ils tueront le rescapé avant la fin du rituel, mais ça suffira à faire passer le Dieu très ancien, en une espèce de forme humanoïde dégoulinante de chaire et de sang.

## Interlude

Bien des années plus tard, les investigateurs sont désormais des adultes bien installés dans la vie.

Ils se retrouveront sur une île au large de Roanoke, dans un manoir appartenant à un vieil homme grabataire en fauteuil roulant. Ce n'est qu'un prétexte. Le vieil homme n'est autre que le Dieu très ancien ayant réussi à reprendre forme humaine. Il a attiré les investigateurs pour tuer les seuls humains ayant menacé son retour. Il en profite pour récupérer les dernières forces qui lui manquaient.

Alors que les investigateurs gisent mourrant sur le sol, il leur montre le passé avant de les laisser mourrir.

## Partie 3

Les investigateurs revivent les évènements de Roanoke via les yeux des investigateurs de l'époque. Ils se retrouvent à la fin prisonniers de leur esprit alors qu'ils vivent les tourments des colons de Roanoke.

## Partie 4

Quelques années plus tard, des enfants devront faire face au Dieu très ancien. Cette partie m'est pour l'instant totalement obscure.