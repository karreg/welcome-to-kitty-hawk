---
date: 2023-09-21
tags:
  - background
  - personnage
---

# Des motivations du rescapé

Quelque chose me gêne dans la trame imaginée jusqu'à présent... Quelles sont les motivations du _[[Le rescapé|rescapé]]_ ?

<!-- more -->

Je veux dire... Il essaie, depuis plus de 400 ans, de faire revenir la colonie. Il a fait des choses horribles, n'hésite pas à tuer pour parvenir à ses fins. Ça nécessite une volonté extraordinaire, du moins au début, tant que l'esprit n'a pas perdu pied. Qu'est-ce qui fait qu'un enfant verse dans le côté sombre des cultes, sans lâcher l'affaire par lassitude, ou désespoir ?

Je pense qu'il a avancé, sans se compromettre, jusqu'à ouvrir une brèche. Le Dieu très ancien a alors profité de cette brèche pour corrompre l'esprit du _rescapé_, lui donnant une volonté hors norme de libérer Roanoke. Dans l'esprit de ce dernier, c'est pour libérer les colons, mais le Dieu très ancien a d'autres desseins.
