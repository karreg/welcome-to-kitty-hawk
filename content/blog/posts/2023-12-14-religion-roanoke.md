---
date: 2023-12-14
tags:
  - background
  - histoire
---

# Religion des Roanoke

Pour s'intégrer au mieux dans l'histoire, et profiter des ressources historique comme aides de jeu, il faut connaître la religion des Roanoke.

<!-- more -->

Le site du [fort Raleigh](https://www.nps.gov/fora/learn/education/indian-religion.htm) en parle.

En résumé, un Dieu unique existait, qui n'avait pas de nom. Ce Dieu a créé d'autres dieux, tells le soleil, la lune, les étoile, l'eau...

Dans les représentations, les Roanoke utilisaient l'image d'hommes, appelés _Kewasowok_ pour l'occasion, ou _Kewas_ au singulier, et ils étaient priés dans des temples appelés _Machicomuck_.

Pas mal de ressources sont disponibles sur le site de [Roanoke Colonies illuminated](https://digital.lib.ecu.edu/roanokecolonies/) mais avec beaucoup d'erreurs.

Il est cependant possible de les retrouver directement depuis [ECU Digital Collections](https://digital.lib.ecu.edu/search?utf8=%E2%9C%93&q=roanoke).
