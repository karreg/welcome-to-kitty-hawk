# Welcome to Kitty Hawk, NC

[(Version publiée)](https://karreg.gitlab.io/welcome-to-kitty-hawk/).

[Kitty Hawk](https://goo.gl/maps/PCNQaAhcqLsmbTweA) est une ville de Caroline du Nord.

C'est aussi, et surtout, le nom de cette campagne de Cthulhu.

C'est une campagne hommage à Stranger Things, E.T., Stephen King, Les Goonies...

C'est une campagne cinématographique et musicale, offrant de nombreuses scènes de présentation, de scènes hors champs, exploitant les musiques d'époque.

D'époque ?

Oui, La campagne se passera sur plusieurs générations, au gré des périodes musicales : Années 50, années 70, années 80, années 2000... 

Et pourquoi Kitty Hawk ?

Kitty Hawk est une petite ville balnéaire, 3500 habitants en 2020, située au nord de l'île de Roanoke où a disparu la colonie, au nord de Kill Devil Hills au nom évocateur, à l'est de la base d'essais militaires de défense de Harvey Point... Et La Caroline du Nord fait partie de la Bible Belt. Sans oublier le passé colonial et la présence des amérindiens...

Elle contient donc TOUS les éléments nécessaires à notre histoire...
